/*
 * Marcus Vin�cius Pitz
 * Cr�ditos: http://opencv.org/
 */
#include <opencv2/core/core.hpp>
#include <opencv2/contrib/contrib.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/objdetect/objdetect.hpp>

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

using namespace cv;
using namespace std;

string IMAGE_DATABASE  = "C:\\Users\\Marcus\\Documents\\Sistemas multimidias\\Trabalho N2,N3,N4\\Teste Open CV\\Teste Open CV\\images\\";
string IMAGE_EXTENSION = ".JPG";

string PERSON_DATABASE_PATH  = "C:\\Users\\Marcus\\Documents\\Sistemas multimidias\\Trabalho N2,N3,N4\\Teste Open CV\\Teste Open CV\\persons\\";
string PERSON_TEMP_DATABASE_PATH  = "C:\\Users\\Marcus\\Documents\\Sistemas multimidias\\Trabalho N2,N3,N4\\Teste Open CV\\Teste Open CV\\persons\\";
string PERSON_EXTENSION = ".PDAT";
string PERSON_FILE_NAME = "PERSONS";
string PERSON_TEMP_FILE_NAME = "PERSONS_TEMP";
string FN_CSV = "csv.ext";
string** g_listname_t;

int IMAGE_WIDTH  = 32;
int IMAGE_HEIGTH = 32;
char SPACE_KEY   = 32;
char ESCAPE_KEY  = 27;
char ALGORITHM_IMPL;

struct ColorMap {
    int R = 0;
    int G = 0;
    int B = 0;
};

struct Person {
    int id;
    int imageCount;
    string name;
};

void log (char *l) {
    printf("\n");
    printf(l);
    printf("\n");
}

void putTextBox(const string& text, const Rect& where, Mat who,  ColorMap color) {
    string box_text;
    box_text.append(text);
    int pos_x = std::max(where.tl().x - 10, 0);
    int pos_y = std::max(where.tl().y - 10, 0);
    putText(who, box_text, Point(pos_x, pos_y), FONT_HERSHEY_PLAIN, 1.0, CV_RGB(color.R,color.G,color.B), 2.0);
}

Person makePerson(const string& name) {
    Person result;
    char* separator = ";";
    string personDb, personTempDb, newLine;
    personDb.append(PERSON_DATABASE_PATH).append(PERSON_FILE_NAME).append(PERSON_EXTENSION);
    personTempDb.append(PERSON_TEMP_DATABASE_PATH).append(PERSON_TEMP_FILE_NAME).append(PERSON_EXTENSION);
    std::ifstream filein(personDb.c_str(), ifstream::in); //File to read from

    if (!filein) {
        //CREATE
        std::ofstream fileout(personDb.c_str());
        newLine.clear();
        result.id = 0;
        result.imageCount = 1;
        result.name = name;
        newLine.append(name).append(separator).append("1").append("\n");
        fileout << newLine;
        return result;
    } else {
        std::ofstream fileout(personTempDb.c_str()); //Temporary file
        //READ
        bool created = false;
        int id = 0;
        int countIm;
        string nameL, countStr;
        string line;
        while (getline(filein, line)) {
            stringstream converter;
            newLine.clear();
            converter.clear();
            stringstream liness(line);
            getline(liness, nameL, ';');
            getline(liness, countStr);
            countIm = atoi(countStr.c_str());

            if (name == nameL) {
                countIm++;
                created = true;
                result.id = id;
                result.imageCount = countIm;
                result.name = nameL;
            }
            converter << countIm;
            newLine.append(nameL).append(separator).append(converter.str()).append("\n");
            fileout << newLine;
            id++;
        }

        if (!created) {
            newLine.clear();
            newLine.append(name).append(separator).append("1").append("\n");
            result.name = name;
            result.id = id;
            result.imageCount = 1;
            fileout << newLine;
        }
        fileout.close();
    }
    string old_name, new_name;
    filein.close();
    old_name = old_name.append(PERSON_DATABASE_PATH).append(PERSON_FILE_NAME).append(PERSON_EXTENSION);
    new_name = new_name.append(PERSON_DATABASE_PATH).append(PERSON_FILE_NAME).append("_DEL").append(PERSON_EXTENSION);
    //original to del and remove
    rename(old_name.c_str()
         , new_name.c_str());
    remove(new_name.c_str());
    //temp to original
    old_name.clear();
    new_name.clear();
    old_name = old_name.append(PERSON_TEMP_DATABASE_PATH).append(PERSON_TEMP_FILE_NAME).append(PERSON_EXTENSION);
    new_name = new_name.append(PERSON_DATABASE_PATH).append(PERSON_FILE_NAME).append(PERSON_EXTENSION);
    rename(old_name.c_str()
         , new_name.c_str());

    return result;
}

string makePath(const Person& person) {
    stringstream resultPath;
    resultPath << IMAGE_DATABASE << person.name << person.imageCount << IMAGE_EXTENSION;
    string result = resultPath.str();
    resultPath << ";" << person.id << "\n";
    std::ofstream fileout;
    fileout.open(FN_CSV.c_str(), ios::out | ios::app);
    fileout << resultPath.str();
    fileout.close();
    return result;
}

bool saveImageToDisk(const string& personName, Mat faceToSave) {
    bool result = false;
    Person iam = makePerson(personName);
    string pathFile = makePath(iam);
    return imwrite( pathFile, faceToSave );
    cout << "asdsadsad";
    return false;
}

void loadPersonNames() {

    string personDb;
    personDb.append(PERSON_DATABASE_PATH).append(PERSON_FILE_NAME).append(PERSON_EXTENSION);
    std::ifstream filein(personDb.c_str(), ifstream::in); //File to read from

    int i = 1;
    if (filein) {
        g_listname_t = (string**) malloc( sizeof(string*) );


        string nameL;
        string line;
        while (getline(filein, line)) {
            if (i != 1) {
                //realloc
                g_listname_t = (string**) realloc (g_listname_t, (sizeof(string*) * i) );
            }

            stringstream liness(line);
            getline(liness, nameL, ';');
            g_listname_t[i-1] = (string*) malloc(sizeof(string));
            *g_listname_t[i-1] = nameL;
            i++;
        }
    }
}

void read_csv(const string& filename, vector<Mat>& images, vector<int>& labels, char separator = ';') {
    std::ifstream file(filename.c_str(), ifstream::in);
    if (!file) {
        string error_message = "Arquivo inexistente, favor verificar.";
        error_message = error_message + filename.c_str() + "!!";
        cout << error_message;
    }
    string line, path, classlabel, errorMsg;
    while (getline(file, line)) {
        stringstream liness(line);
        getline(liness, path, separator);
        getline(liness, classlabel);
        if(!path.empty() && !classlabel.empty()) {
            images.push_back(imread(path, 0));
            cout << "\nCarregando a imagem: " << path << "\n";
            labels.push_back(atoi(classlabel.c_str()));
        }
    }
}

int makeRecognition() {
    log("Iniciando...");
    loadPersonNames();
    string fn_haar = "C:\\OpenCV\\sources\\data\\haarcascades\\haarcascade_frontalface_default.xml";
    //string fn_haar = "C:\\OpenCV\\sources\\data\\haarcascades\\haarcascade_frontalface_alt.xml";
    int deviceId = 0;			// here is my webcam Id.
    // These vectors hold the images and corresponding labels:
    vector<Mat> images;
    vector<int> labels;
    // Read in the data (fails if no valid input filename is given, but you'll get an error message):
    try {
        read_csv(FN_CSV, images, labels);
    } catch (cv::Exception& e) {
        cerr << "Error opening file \"" << FN_CSV << "\". Reason: " << e.msg << endl;
        // nothing more we can do
        exit(1);
    }
    log("Read: Ok");

    Ptr<FaceRecognizer> model;
    // Create a FaceRecognizer and train it on the given images
    if (ALGORITHM_IMPL == '1') {
        model = createEigenFaceRecognizer();
    } else if (ALGORITHM_IMPL == '2') {
        model = createFisherFaceRecognizer();
    } else if (ALGORITHM_IMPL == '3') {
        model = createLBPHFaceRecognizer();
    } else {
        cout << "\nIncorrect algorith, by default EigenFaces is setted\n";
        model = createEigenFaceRecognizer();
    }

    model->train(images, labels);
    log("Train: Ok");
    CascadeClassifier haar_cascade;
    haar_cascade.load(fn_haar);
    // Get a handle to the Video device:
    VideoCapture cap(deviceId);
    log("Cap: Ok");
    // Check if we can use this device at all:
    if(!cap.isOpened()) {
        cerr << "Capture Device ID " << deviceId << "cannot be opened." << endl;
        return -1;
    }
    log("Cap2: Ok");
    // Holds the current frame from the Video device:
    Mat frame;
    while(1) {
        //Sobre carga de operadores
        cap >> frame;
        // Clone the current frame:
        Mat original = frame.clone();
        // Convert the current frame to grayscale:
        Mat gray;
        cvtColor(original, gray, CV_BGR2GRAY);
        // Find the faces in the frame:
        vector< Rect_<int> > faces;
        haar_cascade.detectMultiScale(gray, faces);
        // At this point you have the position of the faces in
        // faces. Now we'll get the faces, make a prediction and
        // annotate it in the video. Cool or what?
        for(int i = 0; i < faces.size(); i++) {
            // Process face by face:
            Rect face_i = faces[i];
            // Crop the face from the image. So simple with OpenCV C++:
            Mat face = gray(face_i);
            Mat face_resized;
            cv::resize(face, face_resized, Size(IMAGE_WIDTH, IMAGE_HEIGTH), 1.0, 1.0, INTER_CUBIC);

            int prediction = model->predict(face_resized);
            string box_text;
            ColorMap cmp;

			if ( prediction >= 0 && prediction <=1 )
			{
			    box_text = format( "Frequ�ncia realizada para o aluno = " );
				box_text.append( *g_listname_t[prediction] );
				cmp.R = 0;
				cmp.G = 255;
				cmp.B = 0;
			}
            else {
                box_text.append( "Frequ�ncia em andamento..." );
                cmp.R = 255;
				cmp.G = 0;
				cmp.B = 0;
            }

            int pos_x = std::max(face_i.tl().x - 10, 0);
            int pos_y = std::max(face_i.tl().y - 10, 0);
            rectangle(original, face_i, CV_RGB(cmp.R, cmp.G, cmp.B), 1);
            putText(original, box_text, Point(pos_x, pos_y), FONT_HERSHEY_PLAIN, 1.0, CV_RGB(cmp.R, cmp.G, cmp.B), 2.0);
        }
        // Show the result:
        imshow("face_recognizer", original);
        // And display it:
        char key = (char) waitKey(20);
        // Exit this loop on escape:
        if(key == 27)
            break;
    }
}

int makeCaptureFace() {
    char key;
    string fn_haar = "C:\\OpenCV\\sources\\data\\haarcascades\\haarcascade_frontalface_default.xml";
    CascadeClassifier haar_cascade;
    haar_cascade.load(fn_haar);
    VideoCapture cap(0);
    log("Cap: Ok");
    if(!cap.isOpened()) {
        cerr << "Capture cannot be opened." << endl;
        return -1;
    }
    Mat frame;
    while (1) {
        cap >> frame;
        Mat original = frame.clone();
        Mat gray;
        cvtColor(original, gray, CV_BGR2GRAY);
        vector< Rect_<int> > faces;
        //detecta as faces se existirem
        haar_cascade.detectMultiScale(gray, faces);
        for(int i = 0; i < faces.size(); i++) { //alterado para somente um rosto ser capturado
            Rect face_i = faces[i]; //retangulo da face
            Mat face = gray(face_i); //face em escala de cinza
            rectangle(original, face_i, CV_RGB(0, 255,0), 1);

            key = (char) waitKey(10);
            if (key == SPACE_KEY) {
                //Redimensiona
                Mat face_resized;
                cv::resize(face, face_resized, Size(IMAGE_WIDTH, IMAGE_HEIGTH), 1.0, 1.0, INTER_CUBIC);
                cout << "Digite o nome da pessoa. \n";
                string personName;
                cin >> personName;
                if ( saveImageToDisk(personName, face_resized) )
                     cout << "Imagem salva com sucesso!!\n";
                else
                    cout << "Erro ao salvar imagem\n";

            } else {
                string writeT = "Pressione a tecla ESPACO para capturar imagem";
                ColorMap cmp;
                cmp.R=255;
                putTextBox(writeT, face_i, original, cmp);
            }
        }
        imshow("Captura de foto", original);
        key = (char) waitKey(10);
        if(key == ESCAPE_KEY)
            break;
    }
}

int main(int argc, const char *argv[]) {
    cout << "Escolha uma das opcoes abaixo\n";
    cout << "Capturar imagem? __ 1 __ \n";
    cout << "Ativar reconhecimento facial? __ 2 __\n";
    char readIn;
    int stat;
    cin >> readIn;

    if (readIn == '1') {
        cout << "Modo captura de faces esta sendo ativado, aguarde... \n";
        stat = makeCaptureFace();
    } else if (readIn == '3') {
        //MODO PARA DEBUG
        loadPersonNames();
    } else if (readIn == '2') {
        cout << "Escolha o algoritimo, 1-EigenFaces __ 2-FisherFaces __ 3-LBPH\n";
        cin >> ALGORITHM_IMPL;
        cout << "Modo reconhecimento de faces esta sendo ativado, aguarde... \n";
        stat = makeRecognition();
    }
    return stat;
}
